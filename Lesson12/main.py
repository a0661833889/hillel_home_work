# 1. Доопрацюйте всі перевірки на типи даних (x, y в Point, begin, end в Line, etc)
# - зробіть перевірки за допомогою property або класса-дескриптора.
# 2. Доопрацюйте класс Triangle з попередньої домашки наступним чином:
# обʼєкти классу Triangle можна порівнювати між собою (==, !=, >, >=, <, <=) за площею.
# перетворення обʼєкту классу Triangle на стрінг показує координати його вершин у форматі x1, y1 -- x2, y2 -- x3, y3
# print(str(triangle1))
#  > (1,0 -- 5,9 -- 3,3)

from abc import ABC, abstractmethod

class int_float:
    @classmethod
    def verify_point(cls, point):
        if not isinstance(point, (int, float)):
            raise TypeError ("Точка должна быть числом.")

    def __set_name__(self, owner, name):
        self.name = '_'+ name

    def __get__(self, instance, owner):
        return getattr(instance, self.name)

    def __set__(self, instance, value):
        self.verify_point(value)
        setattr(instance, self.name, value)


class Point:
    x = int_float()
    y = int_float()

    def __init__(self, x, y):
        self.x = x
        self.y = y


class Figure(ABC):

    @abstractmethod
    def __init__(self):
        pass

    def area(self):
        pass

    # @abstractmethod
    def length(self):
        pass

    #@abstractmethod
    def Triangle(self):
        pass


class Line(Figure):
    begin = None
    end = None

    def __init__(self, begin, end):
        self.begin = begin
        if not isinstance(begin, Point): raise TypeError("что за фигня")
        self.end = end
        if not isinstance(end, Point): raise TypeError("что за фигня")

    def length(self):
        res = ((self.begin.x - self.end.x) ** 2 + (self.begin.y - self.end.y) ** 2) ** 0.5
        return res


class Triangle(Figure):
    a = None
    b = None
    c = None
    def __init__(self, first_line, second_line, third_line):
        self.first_line = first_line
        if not isinstance(first_line, Line): raise TypeError("что за фигня")
        self.second_line = second_line
        if not isinstance(second_line, Line): raise TypeError("что за фигня")
        self.third_line = third_line
        if not isinstance(third_line, Line): raise TypeError("что за фигня")

    def area(self):
        A = self.first_line.length()
        B = self.second_line.length()
        C = self.third_line.length()
        p = (A + B + C) * 0.5
        S = (p * (p - A) * (p - B) * (p - C)) ** 0.5
        return S

    def __str__(self):
        return f' {self.first_line.begin.x},{self.first_line.begin.y} -- ' \
               f'{self.second_line.begin.x},{self.second_line.begin.y}-- ' \
               f'{self.third_line.begin.x},{self.third_line.begin.y}'

    @classmethod
    def __verify_data(cls, other):
        if not isinstance(other, Triangle):
            raise TypeError

    def __eq__(self, other):
        if self.a == self.__verify_data(other):
            return True


    def __gt__(self, other):
        if self.a > self.__verify_data(other):
            return True

    def __ge__(self, other):
        if self.a >= self.__verify_data(other):
            return True

p1 = Point(0, 0)
p2 = Point(1, 1)
p3 = Point(0, 2)
p4 = Point(2, 1)

my_line_a = Line(p1, p2)
my_line_b = Line(p2, p3)
my_line_c = Line(p1, p3)

my_line_a2 = Line(p1, p4)
my_line_b2 = Line(p4, p3)
my_line_c2 = Line(p1, p3)

my_triangle = Triangle(my_line_a, my_line_b, my_line_c)
my_triangle2 = Triangle(my_line_a2, my_line_b2, my_line_c2)


print(my_triangle.area())
print(my_triangle2.area())

print(my_triangle.area() == my_triangle2.area())
print(my_triangle.area() != my_triangle2.area())
print(my_triangle.area() < my_triangle2.area())
print(my_triangle.area() > my_triangle2.area())
print(my_triangle.area() <= my_triangle2.area())
print(my_triangle.area() >= my_triangle2.area())

print(str(my_triangle))
print(str(my_triangle2))
